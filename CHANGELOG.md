<a name="1.1.1"></a>
## 1.1.1 *(2014-12-29)*

 - Criada a página da espinha contendo os pontos de atendimento;
 - Melhorado a URL das páginas: Cidade > Sítio > Setor > Ponto de atendimento;
 - Criação procedural dos objetos;

<a name="1.1.0"></a>
## 1.1.0 *(2014-12-26)*

 - Integração com D3.js e criação da diretiva dos setores para construção das espinhas;
 - Criação da página da espinha;

<a name="1.0.0"></a>
## 1.0.0 *(2014-12-24)*

 - Início