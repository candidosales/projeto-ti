(function() {
  'use strict';

  angular.module('application', [
    'ui.router',
    'ngAnimate',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ])
    .config(config)
    .run(run)
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }
})();

function buildGalpao(id, title) {
        var galpao = {};
        var setores = 4;
        var qtEspinhas = [10, 9, 9, 10];
        var configEspinha = [10.2, 9.2, 9.2, 10.2];
        
        galpao.id = id;
        galpao.title = title;
        galpao.sectors = [];
        
        for (var i = 0, count = 1, id = 1; i < setores; i++ ,id++) {
            var setor = {
                id: id,
                spines: []
            };
            
            for (var j = 0; j < qtEspinhas[i]; j++, count++) {
                setor.spines[j] = {
                    id: count,
                    name: "Espinha " + count,
                    matrix: configEspinha[i],
                    percentage: 1
                };
            }
            
            galpao.sectors[i] = setor;
        }
        
        return galpao;
    };
var GALPAO = buildGalpao(1, "HADOUKEN ULTIMATE MASTER setAppellationMode(true)");

angular.module('application')
.controller('CidadeController', ['$scope', '$stateParams', '$state', '$http', function ($scope, $stateParams, $state, $http) {
    $scope.cities = [{
       id: 1,
       name: "Teresina",
       percentage: 0.6,
       sites:[{
            id: 1,
            name: "Teste",
            percentage: 0.9
        },{
            id: 2,
            name: "Teste 2",
            percentage: 0.1
        }]
   },{
       id: 2,
       name: "Parnaiba",
       percentage: 0.6,
       sites:[{
            id: 4,
            name: "Teste",
            percentage: 0.9
        }]
   }];
   
}]).controller('SitioController', ['$scope', '$rootScope', '$stateParams', '$state', '$http', function ($scope, $rootScope, $stateParams, $state, $http) {
    console.log("SITIO");
   $scope.id = $stateParams.sitio_id;
   $scope.cidade = $stateParams.cidade_id;
   $rootScope.sites = [GALPAO];

   $scope.sectors = find($scope.id);
    
   function find(id){
    for (var i = 0, len = $scope.sites.length; i < len; i++) {
        if($scope.sites[i].id == id){
            return $scope.sites[i].sectors;
            break;
        } 
    }
    return null;
   }

}]).controller('EspinhaController', ['$scope', '$rootScope', '$stateParams', '$state', '$http', function ($scope, $rootScope, $stateParams, $state, $http) {
    console.log("ESPINHA");
    $scope.sites = [GALPAO];
    
    $scope.id = $stateParams.espinha_id;
    $scope.cidade = $stateParams.cidade_id;
    $scope.sitio = $stateParams.sitio_id;
    $scope.setor = $stateParams.setor_id;
    $scope.espinha = $stateParams.espinha_id;
    
    var sitio = $stateParams.sitio_id;
    var setor = $stateParams.setor_id;
    var espinha = $stateParams.espinha_id;
    
    function getStuff(stuff) {
        var partes = ("" + stuff).split(".");
        return { x: parseInt(partes[0]), y: parseInt(partes[1]) };
    }
    
    function buildPAs(espinha) {
        var pas = [];
        
        var valores = getStuff(espinha.matrix);
        for (var i = 0, count = 1; i < valores.y; i++) {
            for (var j = 0; j < valores.x; j++, count++) {
                pas.push({
                    id: count,
                    posicaoEspinha: count,
                    percentage: Math.random()
                });
            }
        }
        
        espinha.points = pas;
        
        return espinha;
    }
    $scope.spine = buildPAs(find(sitio, setor, espinha));
    
    function find(sitio, setor, espinha) {
        for (var i = 0, len = $scope.sites.length; i < len; i++) {
            console.log("$scope.sites[i].id == sitio -> ", $scope.sites[i].id, sitio);
            
            if ($scope.sites[i].id == sitio) {
                var e = null;
                
                for (var j = 0; j < $scope.sites[i].sectors[setor - 1].spines.length; j++) {
                    if ($scope.sites[i].sectors[setor - 1].spines[j].id == espinha) {
                        e = $scope.sites[i].sectors[setor - 1].spines[j];
                        break;
                    }
                }
                
                return e;
            }
        }
        
        return null;
    }

}]).controller('PontoController', ['$scope', '$stateParams', '$state', '$http', function ($scope, $stateParams, $state, $http) {
  
   $scope.id = $stateParams.ponto_id;
   $scope.sitio = $stateParams.sitio_id;
   $scope.setor = $stateParams.setor_id;
   $scope.espinha = $stateParams.espinha_id;
   $scope.points = [{
                        id: 1,
                        tipo:1,
                        posicaoEspinha: 1,
                        switchNumero: 9,
                        switchPorta: 9,
                        networkHostname: 9,
                        networkMac: 1,
                        numeroSerie:0,
                        patrimonio: 1,
                        serialSistemaOperacional: 9,
                        sistemaOperacional: 9,
                        grupoAtendimento: 9,
                        ramal: 1,
                        ramalIp:0,
                        ramalCallServer: 1,
                        ramalFileServer:1,
                        ramalGateway: 0,
                        ramalVlanId:1,
                    },{
                        id: 2,
                        posicaoEspinha: 2,
                        percentage:0.8
                    }];
                

   $scope.ponto = find($scope.id);
    
   function find(id){
    for (var i = 0, len = $scope.points.length; i < len; i++) {
        if($scope.points[i].id == id){
            return $scope.points[i];
            break;
        } 
    }
    return null;
   }

}]).directive('setor', function(){    
    function link(scope, el, attr){
      console.log(scope.data);
        var dataset = scope.data.spines,
            sitio_id = scope.sitio,
            setor_id = scope.setor,
            cidade_id = scope.cidade,
            padding = 2,
            dataSize = dataset.length
            margin = {top: 20, right: 10, bottom: 20, left: 10},
            width = 200 - margin.left - margin.right,
            heightSpine = 40,
            height = (heightSpine+padding)*(dataSize),
            sector = d3.select(el[0]).append('svg')
                                  .attr("width", width)
                                  .attr("height", height);

        var xScale = d3.scale.linear()
								 .domain([0, d3.max(dataset, function(d) { return d.percentage; })])
								 .range([0, width]);
        var colourScale = d3.scale.linear()
                .domain([0,0.5,1])
                .range(['red','yellow','green'])
                .clamp(true);
        
        sector.selectAll("rect").data(dataset).enter()
        .append("a")
        .attr("xlink:href", function(d){
            return "/#!/cidade/" + cidade_id + "/sitio/" + sitio_id + "/setor/" + setor_id + "/espinha/" + d.id;
        })
               .append("rect")
			   .attr("x", 0)
			   .attr("y", function(d,i) {
			   		return (heightSpine*i)+(padding*i);
			   })
			   .attr("class","spine")
			   .attr("width", function(d){
                    return xScale(d.percentage);
                })
               .attr("height", heightSpine)
               .attr("fill",function(d){
                    return colourScale(d.percentage);
               })
               .attr("stroke",'black')
               .attr("stroke-width",0.5);
    }
    return {
        link: link,
        restrict: 'E',
        scope: { data: '=', cidade: '=', sitio: '=', setor: '=' }
    }
}).directive('espinha', function(){    
    function link(scope, el, attr){
        var dataset = scope.data.points,
            cidade_id = scope.cidade,
            setor_id = scope.setor,
            sitio_id = scope.sitio,
            espinha_id = scope.espinha,
            matrix = scope.data.matrix.toString().split("."),
            larguraMatrix = parseInt(matrix[0]),
            alturaMatrix = parseInt(matrix[1]),
            posicaoAltura = 0,
            padding = 2,
            dataSize = dataset.length
            margin = {top: 20, right: 10, bottom: 20, left: 10},
            heightPoint = 40,
            widthPoint = 40,
            width = (widthPoint+padding)*larguraMatrix,
            height = (heightPoint+padding)*alturaMatrix,
            espinha = d3.select(el[0]).append('svg')
                                  .attr("width", width)
                                  .attr("height", height);
        
        var xScale = d3.scale.linear()
								 .domain([0, d3.max(dataset, function(d) { return d.percentage; })])
								 .range([0, width]);
        
        var colourScale = d3.scale.linear()
                .domain([0,0.5,1])
                .range(['red','yellow','green'])
                .clamp(true);
        
        espinha.selectAll("rect").data(dataset).enter()
                .append("a")
                .attr("xlink:href", function(d){
                    return "/#!/cidade/"+cidade_id+"/sitio/"+sitio_id+"/setor/"+setor_id+"/espinha/"+espinha_id+"/ponto/"+d.id;
                })
               .append("rect")
               .attr("y", function(d,i) {
                    var result = d.posicaoEspinha % larguraMatrix;
                    if(result == 1 && d.posicaoEspinha > larguraMatrix || larguraMatrix == 1 && i > 0){
                        posicaoAltura++;
                    }
                    return (posicaoAltura * heightPoint) + padding;
               })
               .attr("x", function(d,i){
            
                    var result = d.posicaoEspinha % larguraMatrix;            
                    // Chegou ao inicio
                    if(result > 0){
                       return ((result-1) * widthPoint) + padding;
                    }else{
                        return ((larguraMatrix-1) * widthPoint) + padding;
                    }
               })
               .attr("class","spine")
               .attr("width", widthPoint)
               .attr("height", heightPoint)
               .attr("fill",function(d){
                    return colourScale(d.percentage);
               })
               .attr("stroke",'black')
               .attr("stroke-width",0.5);   
    }
    return {
        link: link,
        restrict: 'E',
        scope: { data: '=', cidade: '=', sitio: '=', setor: '=', espinha: '=' }
    }
});
